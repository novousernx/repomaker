# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Seokyong Jung <syjung6967@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-09-01 20:54+0000\n"
"PO-Revision-Date: 2020-04-13 22:11+0000\n"
"Last-Translator: Seokyong Jung <syjung6967@gmail.com>\n"
"Language-Team: Korean <https://hosted.weblate.org/projects/f-droid/repomaker/ko/>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.0-dev\n"

#, python-format
msgid "This file is not an update for %s"
msgstr "이 파일은 %s를 위한 업데이트가 아닙니다"

msgid "This APK already exists in the current repo."
msgstr "이 APK는 이미 현재 저장소에서 존재합니다."

msgid "Invalid APK signature"
msgstr "잘못된 APK 서명"

msgid "Invalid APK."
msgstr "잘못된 APK."

msgid "Unsupported File Type"
msgstr "지원되지 않는 파일 형식"

msgid "This file is of a different type than the other versions of this app."
msgstr "이 파일은 다른 버전의 이 앱과 다른 형식입니다."

msgid "APK"
msgstr "APK"

msgid "Book"
msgstr "책"

msgid "Document"
msgstr "문서"

msgid "Image"
msgstr "그림"

msgid "Audio"
msgstr "소리"

msgid "Video"
msgstr "동영상"

msgid "Other"
msgstr "기타"

msgid "This app does already exist in your repository."
msgstr "이 앱은 이미 당신의 저장소에서 존재합니다."

msgid "This app does not have any working versions available."
msgstr "이 앱에 사용 가능한 어떠한 작동 중인 버전도 없습니다."

msgid "Phone"
msgstr "전화"

msgid "7'' Tablet"
msgstr "7'' 태블릿"

msgid "10'' Tablet"
msgstr "10'' 태블릿"

msgid "TV"
msgstr "TV"

msgid "Wearable"
msgstr "웨어러블"

msgid "US Standard"
msgstr "미국 표준"

msgid "Amazon S3 Storage"
msgstr "Amazon S3 저장공간"

msgid "Enter a valid user name consisting of letters, numbers, underscores or hyphens."
msgstr "문자, 숫자, 밑줄과 붙임표로 구성된 올바른 사용자 이름을 입력하세요."

msgid "Enter a valid hostname."
msgstr "올바른 호스트이름을 입력하세요."

msgid "Enter a valid path."
msgstr "올바른 경로를 입력하세요."

msgid "SSH Storage"
msgstr "SSH 저장공간"

msgid "Git Storage"
msgstr "Git 저장공간"

msgid "Default Storage"
msgstr "기본 저장공간"

msgid "American English"
msgstr "미국 영어"

msgid "Change E-mail"
msgstr "이메일 변경"

msgid "Sign Out"
msgstr "로그아웃"

msgid "Sign In"
msgstr "로그인"

msgid "Sign Up"
msgstr "가입하기"

msgid "Distribute apps."
msgstr "앱을 배포합니다."

msgid "Spread the love."
msgstr "사랑을 전파합니다."

msgid "Repomaker is a free web app built for creating collections of apps, music, books, videos and pictures to share with your peers. It uses F-Droid as the mobile distribution platform. Available on Android only."
msgstr "저장소제작기는 앱, 음악, 책, 동영상과 사진의 모음집을 만들어 여러분의 동료와 공유하기 위해 만들어진 자유 웹 앱압니다. 그것은 모바일 배포 플랫폼으로 F-Droid를 사용합니다. Android에서만 사용 가능합니다."

msgid "Login"
msgstr "로그인"

msgid "Forgot Password"
msgstr "비밀번호를 잊음"

msgid "Or login with"
msgstr "또는 다음 방법으로 로그인"

msgid "Logout"
msgstr "로그아웃"

msgid "See you later"
msgstr "나중에 뵙겠습니다"

msgid "Your security is important to us. Please confirm that you want to logout."
msgstr "당신의 보언은 우리에게 중요합니다. 당신이 로그아웃을 하고자 하는지 확인해주세요."

msgid "logout"
msgstr "로그아웃"

msgid "submit"
msgstr "제출"

msgid "We have sent you an email with a link to reset your password."
msgstr "우리는 당신에게 당신의 비밀번호를 재설정할 링크로 된 이메일을 보냈습니다."

msgid "Signup"
msgstr "가입"

msgid "Delete App Version"
msgstr "앱 버전 삭제"

#, python-format
msgid "Are you sure you want to delete this version <strong>%(version)s</strong> (%(code)s) from your app <strong>%(app)s</strong>?"
msgstr "정말 당신의 앱 <strong>%(app)s</strong>에서 이 버전 <strong>%(version)s</strong> (%(code)s)을 삭제하기를 원합니까?"

msgid "Confirm"
msgstr "확정"

msgid "Delete App"
msgstr "앱 삭제"

#, python-format
msgid "Are you sure you want to delete your app <strong>%(object)s</strong>?"
msgstr "정말 당신의 앱 <strong>%(object)s</strong>을 삭제하기를 원합니까?"

#, python-format
msgid "Edit %(app)s"
msgstr "%(app)s 편집"

msgid "Remove App"
msgstr "앱 제거"

msgid "Done"
msgstr "완료"

msgid "By"
msgstr "제작자"

msgid "Summary Override (40 characters)"
msgstr "요약 재정의 (40자)"

msgid "Short Summary (40 characters)"
msgstr "짧은 요약 (40자)"

msgid "Category"
msgstr "카테고리"

msgid "Choose Category"
msgstr "카테고리 선택"

msgid "Description Override"
msgstr "설명 재정의"

msgid "Description"
msgstr "설명"

msgid "Screenshots"
msgstr "스크린샷"

msgid "Add files"
msgstr "파일 추가"

msgid "Drag and drop screenshots"
msgstr "스크린샷을 끌어서 놓기"

msgid "or"
msgstr "또는"

msgid "browse to upload"
msgstr "업로드를 찾아보기"

msgid "Feature Graphic"
msgstr "기능 그래픽"

msgid "Drag and drop a feature graphic"
msgstr "기능 그래픽을 끌어서 놓기"

msgid "Drag and drop .apk files"
msgstr ".apk 파일을 끌어서 놓기"

msgid "Back"
msgstr "뒤로"

msgid "Editing Disabled"
msgstr "편집하기가 비활성화됨"

msgid "This app gets updated automatically from the remote repo. If you want to edit it yourself, you need to disable automatic updates first. Please note that without automatic updates, you will need to add new versions manually."
msgstr "이 앱은 원격 저장소에서 자동으로 업데이트됩니다. 여러분 스스로 그것을 편집하고자 한다면, 여러분은 자동 업데이트를 먼저 비활성화해야 합니다. 자동 업데이트 없이는, 여러분이 새로운 버전을 수동으로 추가해야할 것임을 참고해주세요."

msgid "Disable Automatic Updates"
msgstr "자동 업데이트를 비활성화"

msgid "Delete Feature Graphic"
msgstr "기능 그래픽 삭제"

#, python-format
msgid "Are you sure you want to delete the feature graphic from your app <strong>%(app)s</strong>?"
msgstr "정말 당신의 앱 <strong>%(app)s</strong>에서 기능 그래픽을 삭제하기를 원합니까?"

#, python-format
msgid "Languages (%(count)s)"
msgstr "언어 (%(count)s)"

msgid "Edit"
msgstr "편집"

msgid "Add Translation"
msgstr "번역 추가"

#, python-format
msgid "By %(author)s"
msgstr "%(author)s 제작"

#, python-format
msgid "Updated %(time)s ago"
msgstr "%(time)s 전에 업데이트됨"

#, python-format
msgid "This app gets automatically updated from the remote repo \"%(repo_name)s\"."
msgstr "이 앱은 원격 저장소 \"%(repo_name)s\"에서 자동으로 업데이트됩니다."

msgid "version history"
msgstr "버전 내역"

#, python-format
msgid "Version %(version)s (%(code)s)"
msgstr "버전 %(version)s (%(code)s)"

#, python-format
msgid "Released %(date)s"
msgstr "%(date)s에 배포됨"

msgid "APK is still downloading..."
msgstr "APK가 아직 다운로드 중입니다..."

msgid "This app has no APK files."
msgstr "이 앱에는 APK 파일이 없습니다."

msgid "If you added this app from a remote repo, try deleting it and adding it again."
msgstr "이 앱을 원격 저장소에서 추가했다면, 그것을 삭제하고 그것을 다시 추가하기를 시도하세요."

msgid "Previous"
msgstr "이전"

msgid "Next"
msgstr "다음"

msgid "Add"
msgstr "추가"

msgid "There are currently no screenshots shown because this gives the repo owner the ability to track you."
msgstr "현재 보여지는 스크린샷은 저장소 소유자가 당신을 추적할 가능성이 주어지기 때문에 없습니다."

msgid "show screenshots"
msgstr "스크린샷 보이기"

msgid "Delete Screenshot"
msgstr "스크린샷 삭제"

#, python-format
msgid "Are you sure you want to delete this screenshot from your app <strong>%(app)s</strong>?"
msgstr "정말 당신의 앱 <strong>%(app)s</strong>에서 이 스크린샷을 삭제하기를 원합니까?"

msgid "Language Code"
msgstr "언어 코드"

#, python-format
msgid "Hello, %(user)s"
msgstr "안녕하세요, %(user)s"

msgid "Please Wait!"
msgstr "기다려주세요!"

msgid "Background Operation in Progress"
msgstr "진행 중인 백그라운드 연산"

msgid "Repomaker might be updating or publishing your repo. Or it might be updating a remote repo. Depending on the type and size of the background operation, this might take awhile."
msgstr "저장소제작기는 여러분의 저장소를 업데이트하거나 게시하는 중일 수 있습니다. 또는 그것이 원격 저장소를 엽데이트하는 중일 수 있습니다. 백그라운드 작업의 형식과 크기에 의존하여, 이것은 다소 시간이 걸릴 수 있습니다."

msgid "Try Again"
msgstr "다시 시도"

msgid "The button won't work for you. Please press F5 to try again."
msgstr "버튼이 당신을 위해 작동하지 않습니다. F5를 눌러 다시 시도해주세요."

msgid "Error"
msgstr "오류"

msgid "My Repos"
msgstr "내 저장소"

#, python-format
msgid "Published %(time_ago)s"
msgstr "%(time_ago)s에 배포됨"

#, python-format
msgid "Created %(time_ago)s"
msgstr "%(time_ago)s에 만듦"

msgid "Make a collection of apps to share"
msgstr "공유할 앱의 모음집을 만들기"

msgid "Create Repo"
msgstr "저장소 만들기"

msgid "New Repo"
msgstr "새 저장소"

msgid "Create"
msgstr "만들기"

msgid "Delete Repo"
msgstr "저장소 삭제"

#, python-format
msgid "Are you sure you want to delete the repo <strong>%(repo)s</strong>?"
msgstr "정말 <strong>%(repo)s</strong> 저장소를 삭제하기를 원합니까?"

msgid "This will also delete all apps and other files from your repo. You will not be able to get them back, users of your repo will not be able to use it anymore and not receive updates for the apps they received from you. There is no way to recover this repo!"
msgstr "이것은 또한 모든 앱과 기타 파일을 여러분의 저장소에서 삭제할 것입니다. 여러분은 그들을 되돌려줄 수 없을 것이며, 여러분의 저장소의 사용자는 더 이상 그것을 사용할 수 없고 여러분으로부터 받은 앱을 위한 업데이트를 받지 못할 것입니다. 이 저장소를 복구하는 방법은 없습니다!"

msgid "Please only proceed if you know what you are doing!"
msgstr "당신이 하는 것을 아는 경우에만 진행해주세요!"

msgid "Yes, I want to delete the repo"
msgstr "예, 저는 저장소를 삭제하기를 원합니다"

msgid "Edit Repo"
msgstr "저장소 편집"

msgid "Save Changes"
msgstr "변경사항 저장"

msgid "Content"
msgstr "내용"

msgid "Share"
msgstr "공유"

msgid "Info"
msgstr "정보"

msgid "Search for apps in your repository"
msgstr "당신의 저장소에서 앱을 검색"

msgid "add from gallery"
msgstr "갤러리에서 추가"

#, python-format
msgid "Alternatively, drag and drop or %(label_start)schoose files%(label_end)s"
msgstr "대안으로, 끌어서 놓거나 %(label_start)s파일을 선택하세요%(label_end)s"

msgid "Accepted files include Android apps, documents, eBooks, audio and video."
msgstr "허용되는 파일에는 Android 앱, 문서, eBook, 소리와 동영상을 포함합니다."

msgid "Your search did not return any results."
msgstr "당신의 검색은 어떠한 결과도 반환하지 않았습니다."

msgid "Name"
msgstr "이름"

msgid "Storage"
msgstr "저장공간"

msgid "Add Storage"
msgstr "저장공간 추가"

msgid "Fingerprint"
msgstr "핑거프린트"

msgid "Remove Repo"
msgstr "저장소 제거"

msgid "Add storage to publish"
msgstr "배포할 저장공간 추가"

msgid "Your repo will be automatically published when you add storage. Repomaker does not publically promote your stuff. Only people who receive the direct link to your repo will be able to see it."
msgstr "여러분의 저장소는 여러분이 저장공간을 추가할 때 자동으로 게시될 것입니다. 저장소제작기는 여러분의 소지품을 공개적으로 홍보하지 않습니다. 여러분의 저장소로의 직접 링크를 받은 사람들만이 그것을 볼 수 있을 것입니다."

msgid "add storage"
msgstr "저장공간 추가"

#, python-format
msgid "You have  <span>%(app_count)s app</span> in your repo. Share it!"
msgid_plural "You have  <span>%(app_count)s apps</span> in your repo. Share it!"
msgstr[0] "당신의 저장소에 <span>앱 %(app_count)s개</span>가 있습니다. 그것을 공유하세요!"

msgid "view repo"
msgstr "저장소 보기"

msgid "share public link"
msgstr "공개 링크 공유"

msgid "copy link"
msgstr "링크 복사"

msgid "add to an android phone"
msgstr "android 전화에 추가"

msgid "To install these apps on your Android phone, add them to F-Droid."
msgstr "여러분의 Android 전화에 이러한 앱을 설치하려면, F-Droid에 그들을 추가하세요."

msgid "F-Droid ensures access to secure app updates."
msgstr "F-Droid는 앱 업데이트의 보안 접근을 보장합니다."

msgid "Scan QR"
msgstr "QR 스캔"

#, python-format
msgid "Check out this F-Droid repo: %(name)s"
msgstr "이 F-Droid 저장소를 확인하세요: %(name)s"

msgid "Browse Gallery"
msgstr "갤러리 찾아보기"

msgid "Search"
msgstr "검색"

#, python-format
msgid "Category (%(count)s)"
msgstr "카테고리 (%(count)s)"

msgid "All"
msgstr "모두"

#, python-format
msgid "Source (%(count)s)"
msgstr "출처 (%(count)s)"

msgid "Add a repo"
msgstr "저장소 추가"

msgid "Clear filters"
msgstr "필터 지우기"

#, python-format
msgid "No apps found that match '%(search_term)s'. Please try a different search or remove other filter criteria."
msgstr "'%(search_term)s'와 일치하는 앱을 찾을 수 없습니다. 다른 검색을 시도하거나 다른 필터 기준을 제거해주세요."

msgid "There are no apps in this repo."
msgstr "이 저장소에서 앱이 없습니다."

msgid "There are no apps available in the repos."
msgstr "저장소에서 사용 가능한 앱이 없습니다."

msgid "If you just added a repo, wait for the apps to download and try again later."
msgstr "당신이 그냥 저장소를 추가했다면, 다운로드할 앱을 기다리고 나중에 다시 시도하세요."

msgid "This repo has no apps in the selected category. Try clearing it or select a different repo."
msgstr "이 저장소에는 선택된 카테고리에 앱이 없습니다. 그것을 지워보거나 다른 저장소를 선택하세요."

msgid "There are no apps available in the selected category. Try clearing it."
msgstr "여기에는 선택된 카테고리에 사용 가능한 앱이 없습니다. 그것을 지워보세요."

msgid "Delete Storage"
msgstr "저장공간 삭제"

#, python-format
msgid "Are you sure you want to remove the storage at <strong>%(object)s</strong> from your repo?"
msgstr "정말 당신의 저장소로부터 <strong>%(object)s</strong>에서 저장공간을 제거하기를 원합니까?"

msgid "This will not remove the content from that storage but will stop to publish future versions of your repo to it."
msgstr "이것은 해당 저장공간에서 내용을 제거하지 않을 것이지만 당신의 저장소의 향후 버전에 내용을 게시허는 것을 멈출 것입니다."

msgid "Enable Storage"
msgstr "저장공간 활성화"

msgid "Disable Storage"
msgstr "저장공간 비활성화"

msgid "Repo Address"
msgstr "저장소 주소"

msgid "Edit Storage"
msgstr "저장공간 편집"

msgid "Please make sure to add this public SSH key to your Git account for the upload to work. There are instructions for <a href=\"https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html#how-to-create-your-ssh-keys\">Gitlab</a> and <a href=\"https://developer.github.com/v3/guides/managing-deploy-keys/#deploy-keys\">GitHub</a>."
msgstr "이 공개 SSH 키를 여러분의 Git 계정에 추가하여 업로드가 작동할 수 있도록 해주세요. <a href=\"https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html#how-to-create-your-ssh-keys\">Gitlab</a>과 <a href=\"https://developer.github.com/v3/guides/managing-deploy-keys/#deploy-keys\">GitHub</a>에 지침이 있습니다."

msgid "Also, please make sure that the master branch of your repository is <b>not protected</b> from force pushing."
msgstr "또한, 당신의 저장소의 master 브랜치가 강제 게시로부터 <b>보호되지 않았는지</b> 확인해주세요."

msgid "Git Repository SSH Address"
msgstr "Git 저장소 SSH 주소"

msgid "Region"
msgstr "국가지구"

msgid "Bucket"
msgstr "버킷"

msgid "Access Key ID"
msgstr "액세스 키 ID"

msgid "Please make sure to add this public SSH key to your SSH server's <tt> <a href=\"https://en.wikibooks.org/wiki/OpenSSH/Client_Configuration_Files#.7E.2F.ssh.2Fauthorized_keys\">~/.ssh/authorized_keys</a> </tt> file,\tif you have not done so already."
msgstr "아직 여러분이 완료하지 않았다면, 이 공개 SSH 키를 여러분의 SSH 서버의 <tt> <a href=\"https://en.wikibooks.org/wiki/OpenSSH/Client_Configuration_Files#.7E.2F.ssh.2Fauthorized_keys\">~/.ssh/authorized_keys</a> </tt> 파일에 추가해주시기 바랍니다."

msgid "Security Warning: This gives repomaker write access to that storage. Please limit this user's access as much as possible."
msgstr "보안 경고: 이것은 저장소제작기에 쓰기 권한을 해당 저장공간에 부여합니다. 이 사용자의 접근을 가능하면 최대한 제한해주세요."

msgid "Username"
msgstr "사용자이름"

msgid "Host"
msgstr "호스트"

msgid "Path"
msgstr "경로"

msgid "Your local default SSH key will be used to connect to the storage."
msgstr "당신의 로컬 기본 SSH 키는 저장공간에 연결하는 데 사용될 것입니다."

msgid "Please make sure to authorize this public SSH key for your remote storage, if you have not done so already."
msgstr "아직 여러분이 완료하지 않았다면, 이 공개 SSH 키를 여러분의 저장공간을 위해 인증할 수 있도록 해주세요."

msgid "Activate this storage only once you have added the SSH key."
msgstr "SSH 키를 추가시키고 나서만 이 저장공간을 활성화하세요."

msgid "If this storage is experiencing problems, please check that the SSH key works properly."
msgstr "이 저장공간에 문제를 겪고 있다면, SSH가 제대로 작동하는지 확인해주세요."

#, python-format
msgid "Update %(storage_name)s"
msgstr "%(storage_name)s 업데이트"

#, python-format
msgid "Setup %(storage_name)s"
msgstr "%(storage_name)s 설정"

msgid "cancel"
msgstr "취소"

msgid "save"
msgstr "저장"

msgid "Here's how to copy the SSH Address from GitLab:"
msgstr "여기에 GitLab으로부터 SSH 주소를 복사하는 방법이 있습니다:"

msgid "Setup a place to store your repo"
msgstr "당신의 저장소를 저장할 위치를 설정"

msgid "Git"
msgstr "Git"

msgid "Easy, free, requires an account"
msgstr "쉽고, 무료이며, 계정이 필요합니다"

msgid "Git is a popular tool to version control things. You can securely publish repos to GitLab, GitHub or any other Git service. Git services use SSH to securely copy files from your computer to remote computers."
msgstr "Git은 버전 관리에 널리 사용되는 도구입니다. GitLab, GitHub 등의 Git 서비스에 저장소를 안전하게 게시할 수 있습니다. Git 서비스는 SSH를 사용하여 컴퓨터에서 원격 컴퓨터로 파일을 안전하게 복사합니다."

msgid "Setup"
msgstr "설정"

msgid "GitLab"
msgstr "GitLab"

msgid "GitHub"
msgstr "GitHub"

msgid "SSH"
msgstr "SSH"

msgid "Requires a personal web server or host that supports SSH"
msgstr "개인 웹 서버 또는 SSH를 지원하는 호스트 필요"

msgid "SSH is a technology that can be used to copy files securely to remote computers. When using Git as storage, the files will also be copied via SSH. But the pure SSH option directly copies the files without using any other technology."
msgstr "SSH는 파일을 원격 컴퓨터에 안전하게 복사하기 위해 사용하는 기술입니다. Git을 스토리지로 사용하는 경우 파일은 SSH를 통해 복사되기도 합니다. 그러나 SSH 자체는 다른 기술에 의존하지 않고 파일을 직접 복사합니다."

msgid "Amazon S3"
msgstr "Amazon S3"

msgid "Requires an Amazon account, Pricing varies"
msgstr "Amazon 계정이 필요합니다, 가격은 다양합니다"

msgid "Amazon S3 is a web service offered by Amazon Web Services. Amazon S3 provides storages through web service interfaces. Pricing varies. Visit the Amazon S3 website for details."
msgstr "Amazon S3는 Amazon Web Services에 의해 제공되는 웹 서비스입니다. Amazon S3는 웹 서비스 인터페이스를 통해 저장공간을 제공합니다. 가격은 다양합니다. 자세한 사항은 Amazon S3 웹사이트를 방문하세요."

#, python-format
msgid "Version %(version)s (%(date)s)"
msgstr "버전 %(version)s (%(date)s)"

msgid "Added"
msgstr "추가됨"

msgid "There was an error creating the repository. Please try again!"
msgstr "저장소를 만드는 중 오류가 있었습니다. 다시 시도해주세요!"

msgid "Enter email"
msgstr "이메일 입력"

msgid "Enter your email address and we'll send a link to reset it. If you did not sign up with an email, we cannot help you securely reset your password."
msgstr "당신의 이메일 주소를 입력하면 우리는 그것을 재설정할 수 있는 링크를 보내드립니다. 당신이 이메일로 가입하지 않았다면, 우리는 당신에게 안전하게 당신의 비밀번호를 재설정할 수 없습니다."

msgid "This is not a valid language code."
msgstr "이것은 올바른 언어 코드가 아닙니다."

msgid "This language already exists. Please choose another one!"
msgstr "이 언어는 이미 존재합니다. 다른 것을 선택해주세요!"

msgid "Enter SSH URL"
msgstr "SSH URL 입력"

msgid "Raw Git URL"
msgstr "원본 Git URL"

msgid "This is the location where the uploaded files can be accessed from in raw form. Leave empty for GitHub or GitLab.com repositories."
msgstr "이것은 업로드된 파일이 원시 형식으로 접근될 수 있는 위치입니다. GitHub 또는 GitLab.com 저장소들을 위해 비워 두세요."

msgid "This URL must start with 'git@'."
msgstr "이 URL은 'git@'으로 시작해야 합니다."

msgid "This URL must end with '.git'."
msgstr "이 URL은 '.git'으로 끝나야 합니다."

msgid "This URL is invalid. Please copy the exact SSH URL of your git repository."
msgstr "이 URL은 잘못되었습니다. 당신의 git 저장소의 정확한 SSH URL을 복사해주세요."

msgid "Please add a valid URLfor the raw content of this git repository."
msgstr "이 git 저장소의 원본 내용을 위한 올바른 URL을 추가해주세요."

msgid "Repository URL"
msgstr "저장소 URL"

msgid "Please use a URL with a fingerprint at the end, so we can validate the authenticity of the repository."
msgstr "끝에 핑거프린트로 된 URL을 사용해주시면, 우리는 저장소의 진위 여부를 검증할 수 있습니다."

msgid "Please don't add one of your own repositories here."
msgstr "여기에 여러분 자신의 저장소 중 하나를 추가하지 말아주세요."

#, python-format
msgid "Could not validate repository: %s"
msgstr "저장소를 검증할 수 없었습니다: %s"

msgid "Describe the repo"
msgstr "저장소 설명"

msgid "Bucket Name"
msgstr "버킷 이름"

msgid "Secret Access Key"
msgstr "비밀 액세스 키"

msgid "Other regions are currently not supported."
msgstr "다른 국가지구는 현재 지원되지 않습니다."

msgid "Language"
msgstr "언어"

msgid "Select Screenshot for upload"
msgstr "업로드할 스크린샷을 선택하세요"

msgid "Use local default SSH Key"
msgstr "로컬 기본 SSH 키를 사용"

msgid "User Name"
msgstr "사용자 이름"

msgid "URL"
msgstr "URL"

msgid "This is the location where the uploaded files can be accessed from."
msgstr "이것은 접근될 수 있는 파일이 업로드되는 위치입니다."

msgid "Primary Storage"
msgstr "1차 저장공간"

msgid "Make this storage the primary storage of the repository."
msgstr "이 저장공간을 저장소의 1차 저장공간으로 만듭니다."
